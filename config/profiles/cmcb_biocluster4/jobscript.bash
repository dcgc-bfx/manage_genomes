#!/bin/bash
# properties = {properties}

# Note: avoid curly braces since they will be interpreted by snakemake or escape them, i.e. use '{{' and '}}'

# fail upon error
set -ef -o pipefail

# do not do core dumps (you cannot imagine how fast 5Tb of data are consumed)
ulimit -c 0

# set maximum number of open files to hard limit (ulimit -Hn
ulimit -n 4096

# fix group write
umask g+w

# Export TMPDIR for the job (slurm does not do this); but will be created in actual job
export TMPDIR="/tmp/deepseq.slurm.$SLURM_JOBID.$SLURM_JOB_PARTITION"
mkdir -p "$TMPDIR"

# set so that it will be deleted on exit
trap 'rm -rf -- "$TMPDIR"' EXIT

# load snakemake module
module purge
module load apps/snakemake
module load apps/singularity

avail_cpus=$SLURM_CPUS_ON_NODE 
partition=$SLURM_JOB_PARTITION

# print some info at the start
echo "###########################################################"
echo "Started at: $(date)"
echo "Host: $(hostname)"
echo "Partition: $partition"
echo "User: $(whoami)"
echo "Available CPUs: $avail_cpus"
echo "Working directory: $(pwd)"
echo "Profile: cmcb_biocluster4"
echo "Temp: $TMPDIR"
echo
echo -n 'Snakemake job: '
cat <<- "EOF"
{exec_job}
EOF
echo -n 'Snakemake properties: '
cat <<- "EOF"
{properties}
EOF
echo "###########################################################"
echo

# run job
{exec_job}
