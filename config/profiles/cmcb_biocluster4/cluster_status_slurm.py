#!/usr/bin/env python3

'''
The MIT License (MIT)

Copyright (c) <2020> <DresdenConceptGenomeCenter>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Use Python Naming Conventions
https://www.python.org/dev/peps/pep-0008/#naming-conventions

contact: andreas.petzold(at)tu-dresden.de
'''


"""
When not using DRMAAA, use this script for job status querying in slurm systems:

    snakemake -j 99 --cluster cluster_submit_slurm.py --cluster-status cluster_status_slurm.py
"""

import subprocess
import argparse
import logging
from shlex import quote
from time import sleep

# parse arguments
parser = argparse.ArgumentParser(description='Queries the status of a slurm job and returns either \'success\' or \'running\' or \'failed\'.')
parser.add_argument('jobid', metavar='12345', type=int, help='An slurm job id.')
parser.add_argument('-l','--loglevel', metavar='INFO', type=str, help='The minimum log level (NOTSET|DEBUG|INFO|WARNING|ERROR|CRITICAL).',choices=['NOTSET','DEBUG','INFO','WARNING','ERROR','CRITICAL'],default='INFO',dest='loglevel')
args = parser.parse_args()

# set logging level
logging.basicConfig(level=args.loglevel)
logger = logging.getLogger("cluster_status_slurm.py")

# validate job id
if 'jobid' not in args or not args.jobid:
    logger.error('No job id provided!')
    logger.debug('Job \'' + jobid + '\' is considered \'failed\'.')
    print('failed')
    exit(0)
else:
    jobid = str(args.jobid)
    
# run sacct command
logger.debug('Query state of job with id \'' + str(jobid) + '\'.')

for i in range(3):
    try:
        # run sacct and parse results
        result = subprocess.run(['sacct', '-P', '-b', '-n', '-j', jobid],stdout=subprocess.PIPE)
        out = result.stdout.decode('utf-8').strip().split("\n")
        status_info = {l.split('|')[0] : l.split('|')[1] for l in out if len(l.split('|')) >= 2}
        
        if jobid not in status_info:
            if i < 2:
                logger.error('Could not find job with id \'' + jobid + '\'- will try again (at most 3 times)!')
                sleep(3)
                pass
            else:
                logger.error('Could not find job with id \'' + jobid + '\'!')
                logger.debug('Job \'' + jobid + '\' is considered \'failed\'.')
                print('failed')
                exit(0)
        else:
            break
    except FileNotFoundError as filenotfound:
        logger.error('Could not find \'sacct\' in path!')
        logger.debug('Job \'' + jobid + '\' is considered \'failed\'.')
        print('failed')
        exit(0)
    except subprocess.CalledProcessError as subprocess_exc:
        if i < 2:
            logger.error('Failed to run \'sacct\': ' + subprocess_exc.output + '(' + grepexc.returncode + ') - will try again (at most 3 times)!')
            sleep(3)
            pass
        else:
            logger.error('Failed to run \'sacct\': ' + subprocess_exc.output + '(' + grepexc.returncode + ')!')
            logger.debug('Job \'' + jobid + '\' is considered \'failed\'.')
            print('failed')
            exit(0)

# return 'success', 'running' or 'failed' depending on job state
state = status_info[jobid]
if state in ['PENDING', 'RUNNING', 'SUSPENDED', 'COMPLETING']:
    ret = 'running'
elif state == 'COMPLETED':
    ret = 'success'
else:
    ret = 'failed'
    
logger.debug('Job \'' + jobid + '\' is considered \'' + ret + '\'.')
print(ret)
exit(0)
