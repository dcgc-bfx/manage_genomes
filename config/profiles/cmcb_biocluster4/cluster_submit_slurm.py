#!/usr/bin/env python3

'''
The MIT License (MIT)

Copyright (c) <2020> <DresdenConceptGenomeCenter>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Use Python Naming Conventions
https://www.python.org/dev/peps/pep-0008/#naming-conventions

contact: andreas.petzold(at)tu-dresden.de
'''

"""
When not using DRMAAA, use this script for job submission in slurm systems:

    snakemake -j 99 --cluster cluster_submit_slurm.py --cluster-status cluster_status_slurm.py

Adapted from: https://groups.google.com/forum/#!topic/snakemake/7cyqAIfgeq4
"""

import re
import os
import subprocess
import argparse
import logging
import shlex
import snakemake.utils

# parse arguments
parser = argparse.ArgumentParser(description='Submit a job script to a slurm cluster system')
parser.add_argument('jobscript', metavar='jobscript.sh', type=str,help='A snakemake job script')
parser.add_argument('-l','--loglevel', metavar='INFO',type=str,help='The minimum log level (NOTSET|DEBUG|INFO|WARNING|ERROR|CRITICAL)',choices=['NOTSET','DEBUG','INFO','WARNING','ERROR','CRITICAL'],default='DEBUG',dest='loglevel')
args = parser.parse_args()

# set logging level
logging.basicConfig(level=args.loglevel)
logger = logging.getLogger("cluster_status_slurm.py")

# validate job script
if 'jobscript' not in args or not args.jobscript:
    logger.error('No job script provided - no job will be submitted!')
    exit(2)
else:
    jobscript = args.jobscript

if not os.path.exists(jobscript):
    logger.error('Job script \'' + jobscript + '\' does not exist - no job will be submitted!')
    exit(2)



# read_job_properties returns the properties of a job including
#   resources defined by the rule
#   cluster configuration (passed via cluster config)
#   threads defined by the rule
# note: each snakemake step is one task on one node
job_properties = snakemake.utils.read_job_properties(jobscript)

job_resources = job_properties['resources']
job_cluster_config = job_properties['cluster']
job_default_threads =  job_properties.get('threads', 1)
job_rule_name = job_properties['rule']

# slurm_configuration will contain the configuration for SGE qsub
slurm_configuration = {}

if 'cpus' in job_cluster_config:
    slurm_configuration['cpus'] = job_cluster_config['cpus']
else:
    slurm_configuration['cpus'] = job_resources.get('cpus', job_default_threads)
    
if 'runtime' in job_cluster_config:
    slurm_configuration['runtime'] = job_cluster_config['runtime']
else:
    slurm_configuration['runtime'] = job_resources.get('runtime', '08:00:00')

if 'mem_mb' in job_cluster_config:
    slurm_configuration['mem_mb'] = int(job_cluster_config['mem_mb'])
else:
    slurm_configuration['mem_mb'] = int(job_resources.get('mem_mb', "1000"))

if 'singularity_mem_mb' in job_cluster_config:
    slurm_configuration['singularity_mem_mb'] = int(job_cluster_config['singularity_mem_mb'])
else:
    slurm_configuration['singularity_mem_mb'] = int(job_resources.get('singularity_mem_mb', "0"))
slurm_configuration['memory'] = str(slurm_configuration['mem_mb'] + slurm_configuration['singularity_mem_mb'])

if 'partition' in job_cluster_config:
    slurm_configuration['partition'] = job_cluster_config['partition']
else:
    slurm_configuration['partition'] = job_resources.get('partition', 'ngs.q')

# directory for slurm logs
slurm_configuration['log_dir'] = 'slurm_logs'
if not os.path.exists(slurm_configuration['log_dir']):
    os.makedirs(slurm_configuration['log_dir'])
#    logger.error('Slurm logs are to be saved in \'' + log_dir + '\' but the directory could not be found! Please create it in the current directory!')
#    exit(2)
slurm_configuration['log_dir'] = os.path.abspath(slurm_configuration['log_dir'])

# slurm name
slurm_configuration['slurm_name'] = os.path.basename(jobscript)

# now submit
submit_cmd = 'sbatch -p {partition} -o {log_dir}/%x.%j.log -e {log_dir}/%x.%j.log --parsable --cpus-per-task={cpus} -t {runtime} --mem={memory}M -J {slurm_name} {script}'.format(script=jobscript, **slurm_configuration)
logger.debug('Submit job for rule \'' + job_rule_name + '\' to cluster: ' + submit_cmd + '\n')

try:
    result = subprocess.run(shlex.split(submit_cmd), stdout=subprocess.PIPE)
except subprocess.CalledProcessError as subprocess_exc:                                                                                                   
    logger.error('Failed run \'sbatch\': ' + subprocess_exc.output + '(' + grepexc.returncode + ') - script will fail!')
    exit(2)

jobid = result.stdout.decode('utf-8').strip()
logger.debug('Rule \'' + job_rule_name + '\' was successfully submitted with job id \'' + jobid + '\'.')
print(jobid)

exit(0)
