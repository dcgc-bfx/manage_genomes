#!/bin/bash

#SUBMIT_CMD_NON_INTERACTIVE bash
#SUBMIT_CMD_INTERACTIVE bash

# fail upon error
set -ef -o pipefail

# umask for permissions
umask 002

# load snakemake and singularity modules
module purge
module load apps/snakemake
module load apps/singularity

# make sure /bin/bash is first in PATH
#export PATH=/bin:$PATH

# snakemake command
set +e
{#snakemake_cmd}
status=$?
set -e

# get log
log=$(ls -Art --color=never .snakemake/log/ | tail -1)
cat .snakemake/log/${log} > {#log_name}

# mail log if email adress was set
mail="{#mail}"
if [ -n "$mail" ]
then
  if [ "$status" == "0" ]
  then
    subject="Snakemake workflow {#jobname} finished sucessfully"
  else
    subject="Snakemake workflow {#jobname} finished with errors"
  fi
  mailx -s "$subject" "$mail" < {#log_name}
fi

# fix permissions in snakemake directory
find .snakemake -user $(whoami) -type d -exec chmod 770 {} >& /dev/null
find .snakemake -user $(whoami) -type d -exec chmod 660 {} >& /dev/null
