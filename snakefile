# Download the genome (fasta) and gtf files from the Ensembl or Refseq ftp sites
# For Ensembl, assembly GRch38, works for release >= 47 (all available species)
# For Ensembl, assembly GRch37, works for release >= 83 (only homo_sapiens is availbale)
# For Refseq, provide the relevant information and it works for all
# In case of already available fasta and gtf files, provide the dir paths in config
# Builds genome indices for various mapping tools such as STAR, bwa, bowtie2, ...
# Convert gtf to refFlat, bed, igv and other formats


import subprocess
import sys

################################
### Define packages to check ###
### and install if missing #####
################################

packages = [
    ("bs4", "beautifulsoup4"),
    ("yaml", "pyyaml")
]

# Function to install missing packages
def install_if_missing(package_name, import_name=None):
    try:
        __import__(import_name or package_name)
    except ImportError:
        print(f"Installing {package_name}...")
        subprocess.check_call([sys.executable, "-m", "pip", "install", package_name])

##################################
### Install packages if needed ###
##################################

for import_name, package_name in packages:
    install_if_missing(package_name, import_name)

########################################
### Import all the required packages ###
########################################

import os
import re
import random
import shutil
import warnings
from os.path import exists, abspath
from collections import Counter
from bs4 import BeautifulSoup
import yaml

#################
## config file ##
#################

configfile: "config.yaml"

########################
## Import all scripts ##
########################

#######################
## Define a function ##
## to filter for the ##
## script files #######
#######################

def get_script_files(directory):
    script_files = []
    for filename in os.listdir(directory):
        if filename.endswith((".py")):     # ".sh",
            script_files.append(os.path.join(directory, filename))
    return script_files

#######################
## Fetch all script ###
## files from the #####
## scripts directory ##
#######################

snakefile_dir = os.path.dirname(os.path.abspath(workflow.snakefile))
script_files = get_script_files(os.path.join(snakefile_dir, "scripts"))

# Include all script files in the workflow
for script in script_files:
    include: script

##################################
## Call the function to add ######
## missing fields to the config ##
## and save it to the config #####
## file in the target location. ##
##################################

add_missing_fields(reference_dict, config)

#################
## Singularity ##
#################

singularity_image = config.get("singularity_base_path", "")
singularity: singularity_image

#############
## Modules ##
#############

module_load_prefix = config.get("module_load_prefix", "")

##################################
## Set seed for reproducibility ##
##################################

random.seed(config["seed"])

##############
## Threads  ##
##############

threads = config["threads"]

resources_mb = config["resources_mb"]

################################
## Base url for fetching data ##
################################

source = config["source"]

if (source == "ensembl"):
    base_url = config[source]["base_url"]

    if base_url is None:
        database = config[source]["database"]

        base_url = "http://ftp.ensemblgenomes.ebi.ac.uk/pub/"
        if database == "primary":
            base_url = "http://ftp.ensembl.org/pub/"
        elif database == "bacteria":
            base_url = base_url + "/bacteria/"
        elif database == "fungi":
            base_url = base_url + "/fungi/"
        elif database == "plants":
            base_url = base_url + "/plants/"
        elif database == "protists":
            base_url = base_url + "/protists/"
        elif database == "metazoa":
            base_url = base_url + "/metazoa/"
        else:
            raise ValueError("Invalid database value")
elif source == "refseq":
    base_url = config["refseq"]["base_url"]
    database = None
elif source == "file":
    database = None
elif source == "None":
    error("Source is required.")
else:
    warnings.warn("This is some different source. Standard ones are: ensembl, refseq, and file. With non standard ones ``I promise no guarantees!``")
    base_url = config[source]["base_url"]
    database = None

###########################################
## Parameters from the config and checks ##
###########################################

###########################################
######## Resources local base path ########
###########################################

###############################
## Downloads and other paths ##
###############################


### fasta
if(config[source]["fasta"]["download"]):
    fasta_download_path = path_checker(
        name = "FASTA download", 
        specific_path = config[source]["fasta"]["local_path"], 
        general_path = config["local"]["path_genome"])
else:
    fasta_path = config["local"].get("path_genome")

    if fasta_path is None:
        raise ValueError("config['local']['path_genome'] is not defined")

    if not os.path.exists(fasta_path):
        raise ValueError(f"config['local']['path_genome'] ({fasta_path}) does not exist")

    fasta_download_path = fasta_path

fasta_download_path = os.path.join(fasta_download_path, "organisms")
os.makedirs(fasta_download_path, exist_ok=True)

### annotation
if(config[source]["annotation"]["download"]):    
    annotation_download_path = path_checker(
        name = "ANNOTATION download", 
        specific_path = config[source]["annotation"]["local_path"], 
        general_path = config["local"]["path_genome"])
else:
    annotation_path = config["local"].get("path_genome")

    if annotation_path is None:
        raise ValueError("config['local']['path_genome'] is not defined")

    if not os.path.exists(annotation_path):
        raise ValueError(f"config['local']['path_genome'] ({annotation_path}) does not exist")

    annotation_download_path = annotation_path

annotation_download_path = os.path.join(annotation_download_path, "organisms")
os.makedirs(annotation_download_path, exist_ok=True)

### other
if(config[source]["other"]["download"]):
    other_download_path = path_checker(name = "OTHER download", specific_path = config[source]["annotation"]["local_path"], general_path = config["local"]["path_genome"])
else:
    other_download_path = os.getcwd()

#other_download_path = os.path.join(other_download_path, "organisms")
#os.makedirs(other_download_path, exist_ok=True)

#############
## Indices ##
#############

### bowtie1 ###
if(config["build_indices"]["bowtie1"]["run"]):
    bowtie1_indices_path = path_checker(name = "BOWTIE1 index", specific_path = config["build_indices"]["bowtie1"]["local_path"], general_path = config["local"]["path_indices"])
else:
    bowtie1_indices_path = os.getcwd()

bowtie1_indices_path = os.path.join(bowtie1_indices_path, "indices", "bowtie1")

### bowtie2 ###
if(config["build_indices"]["bowtie2"]["run"]):
    bowtie2_indices_path = path_checker(name = "BOWTIE2 index", specific_path = config["build_indices"]["bowtie2"]["local_path"], general_path = config["local"]["path_indices"])
else:
    bowtie2_indices_path = os.getcwd()

bowtie2_indices_path = os.path.join(bowtie2_indices_path, "indices", "bowtie2")

### bwa ###
if(config["build_indices"]["bwa"]["run"]):
    bwa_indices_path = path_checker(name = "BWA index", specific_path = config["build_indices"]["bwa"]["local_path"], general_path = config["local"]["path_indices"])
else:
    bwa_indices_path = os.getcwd()

bwa_indices_path = os.path.join(bwa_indices_path, "indices", "bwa")

### cellranger ###
if(config["build_indices"]["cellranger-arc"]["run"]):
    cellranger_indices_path = path_checker(name = "CELLRANGER index", specific_path = config["build_indices"]["cellranger-arc"]["local_path"], general_path = config["local"]["path_indices"])
else:
    cellranger_indices_path = os.getcwd()

cellranger_indices_path = os.path.join(cellranger_indices_path, "indices", "cellranger")

### kallisto ###
if(config["build_indices"]["kallisto"]["run"]):
    kallisto_indices_path = path_checker(name = "KALLISTO index", specific_path = config["build_indices"]["kallisto"]["local_path"], general_path = config["local"]["path_indices"])
else:
    kallisto_indices_path = os.getcwd()

kallisto_indices_path = os.path.join(kallisto_indices_path, "indices", "kallisto")

### star ###
if(config["build_indices"]["star"]["run"]):
    star_indices_path = path_checker(name = "STAR index", specific_path = config["build_indices"]["star"]["local_path"], general_path = config["local"]["path_indices"])
else:
    star_indices_path = os.getcwd()

star_indices_path = os.path.join(star_indices_path, "indices", "star")

### xengsort ###
if(config["build_indices"]["xengsort"]["run"]):
    xengsort_indices_path = path_checker(name = "XENGSORT index", specific_path = config["build_indices"]["xengsort"]["local_path"], general_path = config["local"]["path_indices"])
else:
    xengsort_indices_path = os.getcwd()

xengsort_indices_path = os.path.join(xengsort_indices_path, "indices", "xengsort")


#####################
## GTF derivatives ##
#####################


###########################################
###### Check what combinations exist ######
###########################################

if source == "ensembl":
    working_dict = combination_checks_ensembl(config, base_url)
elif source == "refseq":
    working_dict = combination_checks_refseq(config, base_url)
elif source == "file":
    working_dict = combination_checks_file(config)

###############################
## Check what files are #######
## available for the valid ####
## combinations and patterns ##
###############################

if source == "ensembl":
    downloadable_files = downloadable_ensembl(config, working_dict)
elif source == "refseq":
    downloadable_files = downloadable_refseq(config, working_dict)
elif source == "file":
    downloadable_files = downloadable_file(config, working_dict)

#print(yaml.dump(downloadable_files))

#####################
###### Targets ######
#####################

targets = []

print("\n=================================\n")

for key, values in downloadable_files.items():
    
    assembly = values["assembly"]
    release = values["release"]
    species = values["species"]
    seqtype = values["seqtype"]
    genome_files = values["fasta"]
    anno_files = values["annotation"]
    
    anno_url = values["url_anno"]
    fasta_url = values["url_fasta"]

    if config["source"] == "file":
        print("Provided annotation file:", anno_files, "\n")
        print("Provided fasta file:", genome_files, "\n")
        
        targets.append(os.path.join(fasta_download_path, species, f"{assembly}_{release}_{seqtype}", "genome.fa"))
        targets.append(os.path.join(fasta_download_path, species, f"{assembly}_{release}_{seqtype}", "genome.genome"))
        targets.append(os.path.join(annotation_download_path, species, f"{assembly}_{release}_annotation", "annotation.gtf"))
        targets.append(os.path.join(annotation_download_path, species, f"{assembly}_{release}_annotation", "gtf_validator.txt"))

    else:
        for file in anno_files:
            print("Annotation file to download:", anno_url + "/" + file)

        print("\n")

        for file in genome_files:
            print("Fasta file to download:", fasta_url + "/" + file)

        print("\n")
        
        if(config[source]["fasta"]["download"]):
            targets.append(os.path.join(fasta_download_path, species, f"{assembly}_{release}_{seqtype}", "genome.fa"))
            targets.append(os.path.join(fasta_download_path, species, f"{assembly}_{release}_{seqtype}", "genome.genome"))

        
        if(config[source]["annotation"]["download"]):
            targets.append(os.path.join(annotation_download_path, species, f"{assembly}_{release}_annotation", "annotation.gtf"))


    if(config["build_indices"]["bowtie1"]["run"]):
        targets.append(os.path.join(bowtie1_indices_path, species, f"{assembly}_{release}_{seqtype}", f"{species}.4.ebwtl"))
        targets.append(os.path.join(bowtie1_indices_path, species, f"{assembly}_{release}_{seqtype}", f"{species}.rev.2.ebwtl"))

 
    if(config["build_indices"]["bowtie2"]["run"]):
        targets.append(os.path.join(bowtie2_indices_path, species, f"{assembly}_{release}_{seqtype}", f"{species}.4.bt2l"))
        targets.append(os.path.join(bowtie2_indices_path, species, f"{assembly}_{release}_{seqtype}", f"{species}.rev.2.bt2l"))


    if(config["build_indices"]["bwa"]["run"]):
        targets.append(os.path.join(bwa_indices_path, f"{species}", f"{assembly}_{release}_{seqtype}", f"{species}.sa"))


    if(config["build_indices"]["star"]["run"]):
        targets.append(os.path.join(star_indices_path, f"{species}", f"{assembly}_{release}_{seqtype}", "SA"))


    if(config["build_indices"]["kallisto"]["run"]):
        targets.append(os.path.join(kallisto_indices_path, f"{species}", f"{assembly}_{release}_{seqtype}", f"{species}.idx"))

    
    # cellranger restricts the use of '.' in genome name, so changing the species for the path build
    #species_edited = species.replace(".", "-").replace("_", "-")
    if(config["build_indices"]["cellranger-arc"]["run"]):
        targets.append(os.path.join(cellranger_indices_path, f"{species}", f"{assembly}_{release}_{seqtype}", "reference.json"))
    
    
    if(config["annotation_files"]["gene_transcript_relation"]["create"]):
        targets.append(os.path.join(annotation_download_path, f"{species}", f"{assembly}_{release}_annotation", "gene-transcript.txt"))

    
    if(config["annotation_files"]["bed"]["create"]):
        targets.append(os.path.join(annotation_download_path, f"{species}", f"{assembly}_{release}_annotation", "annotation.bed"))
    

    if(config["annotation_files"]["refflat"]["create"]):
        targets.append(os.path.join(annotation_download_path, f"{species}", f"{assembly}_{release}_annotation", "annotation.refFlat"))

    if(config["annotation_files"]["igv"]["create"]):
        targets.append(os.path.join(annotation_download_path, f"{species}", f"{assembly}_{release}_annotation", "annotation.igv.gtf.idx"))

print("=================================\n")

if(config["build_indices"]["xengsort"]["run"]):
    #if(len(downloadable_files) != 2):
    #    error(f"For xengsort index building, two genomes/fasta files are required. Found {len(downloadable_files)}. {downloadable_files}")
    
    species_list = [file['species'] for file in downloadable_files.values()]
    species_counts = Counter(species_list)

    if len(species_counts) != 2:
        raise ValueError(f"For xengsort index building, two species are required. Found {len(species_counts)} . {species_list}")
    
    host = config["build_indices"]["xengsort"]["host"].replace('_', '.')
    graft = config["build_indices"]["xengsort"]["graft"].replace('_', '.')
    
    host_key = [key for key in downloadable_files if re.search(host, key)][0]
    graft_key = [key for key in downloadable_files if re.search(graft, key)][0]

    target_path = (
        os.path.join(
            xengsort_indices_path, 
            f"{host}_{downloadable_files[host_key]['assembly']}_{downloadable_files[host_key]['release']}_{downloadable_files[host_key]['seqtype']}", 
            f"{graft}_{downloadable_files[graft_key]['assembly']}_{downloadable_files[graft_key]['release']}_{downloadable_files[graft_key]['seqtype']}", 
            "index.hash"
        )
    )
    targets.append(target_path)


targets = list(set(targets))
print('\n\n'.join(map(str, targets)) + '\n\n')

###################
###### Rules ######
###################

rule all:
    input: targets
    
    singularity: None

    shell:
        """
        (
            echo {input}
            for target in {input}; do
                if [ -e "$target" ]
                then
                    if [ -d "$target" ]
                    then
                        chmod 2775 "$target"
                    elif [ -f "$target" ]
                    then
                        dir=$(dirname "$target")
                        find "$dir" -type f -exec chmod 664 {{}} +
                    fi
                fi
            done
        )
        """


################################
## Import all rule files from ##
## the rules directory #########
################################

###############
## Downloads ##
###############

include: "rules/download.smk"


#######################################
## Create ancillary files from fasta ##
#######################################

include: "rules/create_ancillary_files_from_fa.smk"


#####################
## GTF derivatives ##
#####################

include: "rules/bed.smk"
include: "rules/gene_transcript.smk"
include: "rules/igv.smk"
include: "rules/refflat.smk"
include: "rules/cellranger_gtf.smk"

#############
## Indices ##
#############

include: "rules/bowtie1_index.smk"
include: "rules/bowtie2_index.smk"
include: "rules/bwa_index.smk"
include: "rules/cellranger-arc_index.smk"
include: "rules/kallisto_index.smk"
include: "rules/star_index.smk"
include: "rules/xengsort_index.smk"