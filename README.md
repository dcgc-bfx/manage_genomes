# Manage Genomes
This snakemake workflow is written to perform multiple steps to prepare the genomes for further downstream analysis in various other workflows.

The steps performed in this workflow include:

1. Download the genome fasta from Ensembl or Refseq
2. Download the corresponding annotation gtf from Ensembl or Refseq
3. Create indices: bowtie1, bowtie2, bwa, star, cellranger, xengsort
4. Generate various derivatives of gtf: bed, refflat, cellranger gtf, and gene-transcript

| What              | Type            | Status      |
|-------------------|-----------------|-------------|
| Download          |                 |             |
|                   | ensembl fasta   | Done        |
|                   | ensembl gtf     | Done        |
|                   | ensembl others  | to be done  |
|                   | refseq fasta    | Done        |
|                   | refseq gtf      | Done        |
|                   | refseq others   | to be done  |
| Build Indices     |                 |             |
|                   | bwa             | Done        |
|                   | bowtie1         | Done        |
|                   | bowtie2         | Done        |
|                   | star            | Done        |
|                   | cellranger      | Done        |
|                   | xengsort        | to be done  |
| GTF derivatives   |                 |             |
|                   | bed             | Done        |
|                   | cellranger gtf  | Done        |
|                   | gene-transcript | Done        |
|                   | igv             | Done        |
|                   | refFlat         | Done        |
| FASTA derivatives |                 |             |
|                   | genome.dict     | Done        |
|                   | genome.fa.fai   | Done        |
|                   | genome.genome   | Done        |

**Additionally, you can build indices and gtf derivatives from already existing / manually downloaded fasta and gtf files.**

## Usage:

Before you decide to build new genome indices and other files, check what is already done
        # assuming /dcgc/support/references/ is your directory for storing the genome files  
      
        # what species' fasta are downloaded and if their ancillary files are created
        tree -L 1 /dcgc/support/references/organisms/<organism_name>/*_dna/

        # what species' gtf are downloaded and if their derivative files are created
        tree -L 1 /dcgc/support/references/organisms/<organism_name>/*_annotation/
        
        # what indices are available
        tree -L 2 /dcgc/support/references/indices/<tool_name>/


### Building the indices

Go to the path `/dcgc/support/references` and then execute the snakemake
        
        # Login to cluster
        ssh biocluster4

        # Start screen session
        screen -S manage_genomes

        ########################################
        # Run it on the cluster (preferred) #
        ########################################
        
        # Go to directory and load required modules
        cd /dcgc/support/references/
        module load apps/snakemake
        module load apps/singularity

        # Create a configuration file (see doc below) : build_genome.config
        
        # Then do a dry-run of the pipeline to check everything is okay
        snakemake --snakefile /group/sequencing/bfx/scripts/common/manage_genomes/snakefile \
          --profile /group/sequencing/bfx/scripts/common/manage_genomes/config/profiles/cmcb_biocluster4 \
          --directory ".snakemake_$(whoami)" --configfile build_genome.config --jobs 10 --printshellcmds --dryrun

Always run with --dryrun first to see if the download url for your target species (annotation and fasta) are correctly configured. Look in the terminal just over the `Building DAG of jobs...`. If not, adjust the `file_patterns_include` and `file_patterns_exclude` for annotation and/or fasta. Additionally use --printshellcmds to see what is done in detail.

![download](images/screenshot.png)

When everything looks okay, run

        # Run pipeline
        snakemake --snakefile /group/sequencing/bfx/scripts/common/manage_genomes/snakefile \
          --profile /group/sequencing/bfx/scripts/common/manage_genomes/config/profiles/cmcb_biocluster4 \
          --directory ".snakemake_$(whoami)" --configfile build_genome.config --jobs 10 --retries 3
        
        ############################################################################
        # Either run it on a local node (.e. c-2-3); request 24 cores with 60G RAM #
        ############################################################################

        # Login to a node
        srun --nodes=1 --ntasks=24 --x11 --mem=60G --time=24:00:00 --pty /bin/bash

        # Go to directory and load required modules
        cd /dcgc/support/references/
        module load apps/snakemake
        module load apps/singularity
        
        # Create a configuration file (see doc below) : build_genome.config

        # Make sure you are not on the head node
        hostname

        # Then do a dry-run of the pipeline to check everything is okay
        snakemake --snakefile /group/sequencing/bfx/scripts/common/manage_genomes/snakefile \
          --profile /group/sequencing/bfx/scripts/common/manage_genomes/config/profiles/cmcb_local \
          --directory ".snakemake_$(whoami)" --configfile build_genome.config --cores 24 --dryrun --printshellcmds

        # Run pipeline
        snakemake --snakefile /group/sequencing/bfx/scripts/common/manage_genomes/snakefile \
          --profile /group/sequencing/bfx/scripts/common/manage_genomes/config/profiles/cmcb_local \
          --directory ".snakemake_$(whoami)" --configfile build_genome.config --cores 24


## Configs

When working on a new species, create and save your config. Below are given some examples of the minimum configs; for details check the [**wiki**](https://gitlab.hrz.tu-chemnitz.de/dcgc-bfx/manage_genomes/-/wikis/home). You can also check the existing configs in `/dcgc/support/references/configs/`.

### Example 1: Minimum config Ensembl
This is an example of minimum config for downloading a species from ensembl, building bowtie1 index and creating gene transcript file.

        threads: 10

        local:
                path_genome: "/dcgc/support/references/"
                path_indices: "/dcgc/support/references/"

        source: ensembl

        ensembl:
                database: primary
                assembly: [grch38]
                release: [release-111]
                species: [ciona_intestinalis]
                seq_type: [dna]
                annotation: 
                        download: true
                        file_patterns_include: [gtf]
                        file_patterns_exclude: [abinitio, chromosome, chr.gtf, CHECKSUMS]
                fasta: 
                        download: true
                        file_patterns_include: [sm]
                        file_patterns_exclude: [rm, primary, nonchromosomal, chromosome]

        build_indices:
                bowtie1:
                        run: true

        annotation_files:
                gene_transcript_relation: 
                        create: true

### Example 2: Minimum config Refseq
This is an example of minimum config for downloading a species from refseq, building star index and creating refflat file.

        threads: 10

        local:
                path_genome: "/dcgc/support/references/"
                path_indices: "/dcgc/support/references/"

        source: refseq

        
        refseq: 
                base_url: https://ftp.ncbi.nlm.nih.gov/genomes/all/
                accession: [GCA_001708325.2]
                assembly: [ASM170832v2]
                species: [synthetic_bacterium_jcvi-syn3a]
                seq_type: [dna]
                annotation: 
                        download: true
                        file_patterns_include: [gtf]
                        file_patterns_exclude: [nothing]
                fasta: 
                        download: true
                        file_patterns_include: [fna]
                        file_patterns_exclude: [nothing]

        build_indices:
                star:
                        run: true

        annotation_files:
                refflat: 
                        create: true

### Example 3: Minimum config From files
This is an example of minimum config for using an already downloaded genome fasta (.fa or .fa.gz), annotation (.gtf or .gtf.gz) files, and building kallisto index and creating annotation bed file.

        threads: 10

        local:
                path_genome: "/dcgc/support/references/"
                path_indices: "/dcgc/support/references/"

        source: file

        file:
                fasta_path: GCF_000287275.1_ASM28727v1_rna_from_genomic.fna.gz
                gtf_path: GCF_000287275.1_ASM28727v1_genomic.gtf.gz
                assembly: GCF_000287275.1
                release: 1
                species: candidatus_carsonella
                seq_type: dna
                annotation: 
                        local_path: 
                fasta: 
                        local_path: 

        build_indices:
                kallisto:
                        run: true

        annotation_files:
                bed: 
                        create: true


A good example case for using files for building the indices is when you want to edit the fasta and/or gtf files for specific projects, for instance adding a GFP gene to the fasta file and its annotation to the gtf file. In such a case, first download the target files using the **manage_genomes** or **manually** and edit them and then run the **manage_genomes** to build the indices.

Another example case can be to use the genome from any unconfigured source (other than ensembl and refseq) for example a publication.

## Other notes

## Under development
### Current
- Using official singularity / docker images for compatibility across platforms

### Later
- Xengsort indices
- Download repeats or other from Ensembl (mysql/\*species\*/\*core\*/)
- Download VEP files
- Download ERCC and add to index building
