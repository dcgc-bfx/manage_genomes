"""
Function that prepares module specifications
"""

def format_module_spec(*modules):
    """
    Accepts module names and create the string for loading the 
    modules 
    """

    module_str = ""
    for m in list(modules):
        module_str = module_str + " " + module_load_prefix + m
    return module_str