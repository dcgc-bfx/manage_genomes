"""
Module for checking what files have to be downloaded from the source
"""

import urllib.request
import re
import warnings
from bs4 import BeautifulSoup


#### Colors for printing the available names
# ANSI color escape codes
DARK_RED = "\033[31m"   # Fail message
DARK_GREEN = "\033[32m" # Success message
COLOR_END = "\033[0m"   # Reset color to default

colored_failed_files = f"{DARK_RED}Failed: No files left after filtering for the regex{COLOR_END}"
colored_success_files = f"{DARK_RED}Succeded: Got file(s) to fetch{COLOR_END}"

class MissingFiles(UserWarning):
    """Class representing missing file(s)."""


def downloadable_ensembl(config, combn_dict):
    """Class to check for the available files in ensembl"""

    for key in combn_dict:

        a = combn_dict[key]['assembly']
        r = combn_dict[key]['release']
        s = combn_dict[key]['species']
        t = combn_dict[key]['seqtype']
        url_fasta = combn_dict[key]['url_fasta']
        url_anno = combn_dict[key]['url_anno']

        
        print("\n" + "Checking files for " + a + " " + r + " " + s + " " + t)

        # get fasta files ###########
        #if(config["ensembl"]["fasta"]["download"]):
            
        with urllib.request.urlopen(url_fasta) as response:
            html_content = response.read()

        #print(html_content)

        soup = BeautifulSoup(html_content, "html.parser")
        all_files = list([link['href'] for link in soup.find_all('a') if link.get('href') and not link['href'].endswith('/')])


        # patterns fasta file ########
        fasta_patterns_include = config["ensembl"]["fasta"]["file_patterns_include"]
        fasta_patterns_exclude = config["ensembl"]["fasta"]["file_patterns_exclude"]

        fasta_to_keep = re.compile('|'.join(fasta_patterns_include))
        fasta_to_remove = re.compile('|'.join(fasta_patterns_exclude))

        fasta_download = [s for s in all_files if re.search(fasta_to_keep, s)]
        fasta_download = [s for s in fasta_download if not re.search(fasta_to_remove, s)]

        print("Fasta file(s)")
        if not fasta_download:
            warnings.warn(colored_failed_files, MissingFiles)
            combn_dict[key]["fasta"] = ""
        else:
            print(colored_success_files)
            print(fasta_download)
            combn_dict[key]["fasta"] = fasta_download

        
        # obtaining the assembly from the fasta file names
        if fasta_download:
            assembly = re.sub(re.escape(s) + r'\.', '', fasta_download[0], flags=re.IGNORECASE, count=1).split("." + t)[0].replace("_", "")
            combn_dict[key]['assembly'] = assembly
        # else:
        #     assembly = a


        # get anno files ###########
        with urllib.request.urlopen(url_anno) as response:
            html_content = response.read()

        soup = BeautifulSoup(html_content, "html.parser")
        all_files_anno = list([link['href'] for link in soup.find_all('a') if link.get('href') and not link['href'].endswith('/')])

        # patterns anno file ########
        anno_patterns_include = config["ensembl"]["annotation"]["file_patterns_include"]
        anno_patterns_exclude = config["ensembl"]["annotation"]["file_patterns_exclude"]

        anno_to_keep = re.compile('|'.join(anno_patterns_include))
        anno_to_remove = re.compile('|'.join(anno_patterns_exclude))

        anno_download = [s for s in all_files_anno if re.search(anno_to_keep, s)]
        anno_download = [s for s in anno_download if not re.search(anno_to_remove, s)]

        print("Annotation file(s)")
        if not anno_download:
            warnings.warn(colored_failed_files, MissingFiles)
            combn_dict[key]["annotation"] = ""
        else:
            print(colored_success_files)
            print(anno_download)
            combn_dict[key]["annotation"] = anno_download
    
        #naam = assembly +  "_" + r + "_" + s + "_" + t
        #combn_dict[naam] = combn_dict.pop(key, "test")

    return(combn_dict)


def downloadable_refseq(config, combn_dict):
    """Class to check for the available files in refseq"""

    for key in combn_dict:

        a = combn_dict[key]['assembly']
        r = combn_dict[key]['release']
        s = combn_dict[key]['species']
        t = combn_dict[key]['seqtype']
        url_fasta = combn_dict[key]['url_fasta']
        url_anno = combn_dict[key]['url_anno']
        
        print("\n" + "Checking files for " + a + " " + r + " " + s + " " + t)

        # get fasta files ###########
        with urllib.request.urlopen(url_fasta) as response:
            html_content = response.read()

        #print(html_content)

        soup = BeautifulSoup(html_content, "html.parser")
        all_files = list([link['href'] for link in soup.find_all('a') if link.get('href') and not link['href'].endswith('/')])


        # patterns fasta file ########
        fasta_patterns_include = config["refseq"]["fasta"]["file_patterns_include"]
        fasta_patterns_exclude = config["refseq"]["fasta"]["file_patterns_exclude"]

        if (t == "dna"):
            fasta_patterns_exclude.extend(['cds', 'rna'])


        fasta_to_keep = re.compile('|'.join(fasta_patterns_include))
        fasta_to_remove = re.compile('|'.join(fasta_patterns_exclude))

        fasta_download = [s for s in all_files if re.search(fasta_to_keep, s)]
        fasta_download = [s for s in fasta_download if not re.search(fasta_to_remove, s)]

        print("Fasta file(s)")
        if not fasta_download:
            warnings.warn(colored_failed_files, MissingFiles)
            combn_dict[key]["fasta"] = ""
        else:
            print(colored_success_files)
            combn_dict[key]["fasta"] = fasta_download


        # get anno files ###########
        with urllib.request.urlopen(url_anno) as response:
            html_content = response.read()

        soup = BeautifulSoup(html_content, "html.parser")
        all_files_anno = list([link['href'] for link in soup.find_all('a') if link.get('href') and not link['href'].endswith('/')])

        # patterns anno file ########
        anno_patterns_include = config["refseq"]["annotation"]["file_patterns_include"]
        anno_patterns_exclude = config["refseq"]["annotation"]["file_patterns_exclude"]

        anno_to_keep = re.compile('|'.join(anno_patterns_include))
        anno_to_remove = re.compile('|'.join(anno_patterns_exclude))

        anno_download = [s for s in all_files_anno if re.search(anno_to_keep, s)]
        anno_download = [s for s in anno_download if not re.search(anno_to_remove, s)]

        print("Annotation file(s)")
        if not anno_download:
            warnings.warn(colored_failed_files, MissingFiles)
            combn_dict[key]["annotation"] = ""
        else:
            print(colored_success_files)
            combn_dict[key]["annotation"] = anno_download
    
    return(combn_dict)


def downloadable_file(config, combn_dict):
    """Class to create a similar dict structure as refseq and ensembl when source == file; no checks"""

    for key in combn_dict:

        a = combn_dict[key]['assembly']
        r = combn_dict[key]['release']
        s = combn_dict[key]['species']
        t = combn_dict[key]['seqtype']
        url_fasta = combn_dict[key]['url_fasta']
        url_anno = combn_dict[key]['url_anno']
        
        naam = f"{a}_{r}_{s}_{t}"
        combn_dict[naam] = {
            'assembly': a, 
            'release': r, 
            'species': s, 
            'seqtype': t, 
            'url_fasta': url_fasta, 
            'url_anno': url_anno,
            'fasta': config["file"]["fasta_path"],
            'annotation': config["file"]["gtf_path"]
        }
    
    return(combn_dict)