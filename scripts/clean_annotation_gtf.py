"""Function for cleaning the gtf files for correct usage with collapse_gtf.py"""

def clean_refseq_gtf(input_gtf):
    """
    Processes a tab-delimited file (gtf), modifying a specific column based on certain 
    conditions.

    The function reads an input file line by line. For each line, if the first column does not
    start with a '#', it splits the ninth column on '";', prepends "extra " to parts that do not 
    contain a space, removes extra ';', and then reassembles the column.

    Args:
        input (str): The path to the input gtf file to be processed.

    Returns:
        list
    """

    processed = []

    with open(input_gtf, 'r', encoding='utf-8') as f:
        #for line in csv.reader(f, delimiter='\t'):
        for raw_line in f:
            line = raw_line.strip().split('\t')

            if not line[0].startswith('#'):
                parts = line[8].split('";')
                parts = [part.replace(';', ',') for part in parts]
                line[8] = '"; '.join(parts)

            processed.append(line)

    return(processed)

