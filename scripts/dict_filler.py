"""Reference dictionary with all fields for the config"""
reference_dict = {
    "seed": 123,
    "threads": 10,
    "resources_mb": 50000,
    "local": {
        "path_genome": ".",
        "path_indices": ".",
        "path_other_files": "."
    },
    "source": None,
    "ensembl": {
        "base_url": None,
        "database": "primary",
        "assembly": ["grch38"],
        "release": None,
        "collection": None,
        "species": None,
        "seq_type": None,
        "annotation": {
            "download": False,
            "local_path": None,
            "file_patterns_include": None,
            "file_patterns_exclude": None
        },
        "fasta": {
            "download": False,
            "local_path": None,
            "file_patterns_include": None,
            "file_patterns_exclude": None
        },
        "other": {
            "download": False,
            "local_path": None,
            "file_patterns_include": None,
            "file_patterns_exclude": None
        },
    },
    "refseq": {
        "base_url": 'https://ftp.ncbi.nlm.nih.gov/genomes/all',
        "accession": None,
        "assembly": None,
        "species": None,
        "seq_type": None,
        "annotation": {
            "download": False,
            "local_path": None,
            "file_patterns_include": None,
            "file_patterns_exclude": None
        },
        "fasta": {
            "download": False,
            "local_path": None,
            "file_patterns_include": None,
            "file_patterns_exclude": None
        },
        "other": {
            "download": False,
            "local_path": None,
            "file_patterns_include": None,
            "file_patterns_exclude": None
        }
    },
    "file": {
        "assembly": None,
        "release": None,
        "species": None,
        "seq_type": None,
        "annotation": {
            "download": True,
            "local_path": None
        },
        "fasta": {
            "download": True,
            "local_path": None
        },
        "other": {
            "download": True,
            "local_path": None
        }
    },
    "build_indices": {
        "bowtie1": {
            "run": False,
            "local_path": None,
            "tool_params": None
        },
        "bowtie2": {
            "run": False,
            "local_path": None,
            "tool_params": None
        },
        "bwa": {
            "run": False,
            "local_path": None,
            "tool_params": None
        },
        "cellranger-arc": {
            "run": False,
            "local_path": None,
            "tool_params": None,
            "tf_motifs": None,
            "non_nuclear_contigs": None
        },
        "kallisto": {
            "run": False,
            "local_path": None,
            "type": None,
            "tool_params": None
        },
        "star": {
            "run": False,
            "local_path": None,
            "tool_params": None
        },
        "xengsort": {
            "run": False,
            "local_path": None,
            "tool_params": "-n 100_000_000 -k 31",
            "host": None,
            "graft": None
        }
    },
    "annotation_files": {
        "gene_transcript_relation": {
            "create": False
        },
        "bed": {
            "create": False
        },
        "refflat": {
            "create": False
        },
        "igv": {
            "create": False
        }
    }
}


# Function to recursively add missing fields
def add_missing_fields(ref_dict, existing_dict):
    """Adds the missing (default) data in the config"""
    for key, ref_value in ref_dict.items():
        if isinstance(ref_value, dict):
            # If the key is missing in existing_dict, create it
            if key not in existing_dict:
                existing_dict[key] = {}
            # Recur for nested dictionaries
            add_missing_fields(ref_value, existing_dict[key])
        else:
            # Add missing keys with the value from the reference dict
            if key not in existing_dict:
                existing_dict[key] = ref_value
