"""
Module for checking what paths on the url exist
"""

import shutil
import urllib.request
import warnings
from itertools import product
from bs4 import BeautifulSoup


#### Colors for printing the available names
# ANSI color escape codes
GREEN = "\033[95m"      # assembly
BLUE = "\033[94m"       # release
RED = "\033[91m"        # species
YELLOW = "\033[93m"     # seq type
DARK_RED = "\033[31m"   # Fail message
DARK_GREEN = "\033[32m" # Success message
COLOR_END = "\033[0m"   # Reset color to default

colored_failed = f"{DARK_RED}Failed fetching combination{COLOR_END}"
colored_success = f"{DARK_RED}Succeded fetching combination{COLOR_END}"

class MissingValueCombination(UserWarning):
    """Class representing a missing combination."""


def combination_checks_ensembl(config, base_url):
    """Class to check for the available assemblies/releases/collections/species/seq types in ensembl"""

    all_assemblies = ['grch37', 'grch38']
    
    assembly = config["ensembl"]["assembly"]
    release = config["ensembl"]["release"]
    collection = config["ensembl"]["collection"]
    species = config["ensembl"]["species"]
    seqtype = config["ensembl"]["seq_type"]
    
    spe = "homo_sapiens" in species
    bac = "bacteria" in base_url
    
    if not assembly and spe:
        raise ValueError("Please provide the assembly via 'assembly' in the config.yaml when species is homo_sapiens!")
    
    if not assembly and bac:
        assembly = ["pseudoValue"]

    if not release:
        raise ValueError("Please provide the release via 'release' in the config.yaml!")

    if not collection and bac:
        raise ValueError("Please provide the collection via 'collection' in the config.yaml!")
    
    if not species:
        raise ValueError("Please provide the species via 'species' in the config.yaml!")

    if not seqtype:
        raise ValueError("Please provide the seq type via 'seq_type' in the config.yaml!")
    
    results = {}
    for idx, (a, r, s, t) in enumerate(product(assembly, release, species, seqtype)):
        
        # assembly ############
        if a != "pseudoValue":
            print("\n" + "Running for " + a + " " + r + " " + s + " " + t)
            if a not in all_assemblies:
                print(colored_failed)
                terminal_width = shutil.get_terminal_size().columns
                assembly_per_line = terminal_width // max(len(assembly) for assembly in all_assemblies)
                print("\nAvailable assemblies:")
                for i in range(0, len(all_assemblies), assembly_per_line):
                    assembly = "  ".join(all_assemblies[i:i+assembly_per_line])
                    colored_assembly = f"{GREEN}{assembly}{COLOR_END}"
                    print(colored_assembly)
                warnings.warn(f"The provided assembly '{a}' is not available in Ensembl", MissingValueCombination)
                continue


        if a == 'grch38':
            assembly_path = ''
        elif a == 'grch37':
            assembly_path = 'grch37'
        elif a == 'pseudoValue':
            assembly_path = ''
            a = ''


        # release ############
        url = f"{base_url}/{assembly_path}/"
        with urllib.request.urlopen(url) as response:
            html_content = response.read()

        soup = BeautifulSoup(html_content, "html.parser")
        all_releases = [link.text.strip("/") for link in soup.find_all("a") if "release" in link.text]

        if r not in all_releases:
            print(colored_failed)
            terminal_width = shutil.get_terminal_size().columns
            release_per_line = terminal_width // max(len(release) for release in all_releases)
            print(f"\nAvailable release for assembly '{a}':")
            for i in range(0, len(all_releases), release_per_line):
                release = "  ".join(all_releases[i:i+release_per_line])
                colored_release = f"{BLUE}{release}{COLOR_END}"
                print(colored_release)
            warnings.warn(f"The provided release '{r}' is not available for assembly '{a}' in Ensembl.", MissingValueCombination)
            continue
        
        # collections; bacteria only ########
        available_collection = []
        if bac:
            url = f"{base_url}/{assembly_path}/{r}/fasta/"

            with urllib.request.urlopen(url) as response:
                        html_content = response.read()

            soup = BeautifulSoup(html_content, "html.parser")
            all_collections = [link.text.strip("/") for link in soup.find_all("a") if "/" in link.text and link.text.endswith("/")]

            for c in collection:
                if c not in all_collections:
                    print(colored_failed)
                    terminal_width = shutil.get_terminal_size().columns
                    collection_per_line = terminal_width // max(len(collection) for collection in all_collections)
                    print(f"\nAvailable collection for release '{r}':")
                    for i in range(0, len(all_collections), collection_per_line):
                        collection = "  ".join(all_collections[i:i+collection_per_line])
                        colored_collection = f"{BLUE}{collection}{COLOR_END}"
                        print(colored_collection)
                    warnings.warn(f"The provided collection '{c}' is not available for release '{r}' in Ensembl Bacteria.", MissingValueCombination)
                    continue
                else:
                    available_collection = available_collection + [c]
            
        available_collection = ["pseudoValue"] if not available_collection else available_collection

        for c in available_collection:

            c = "" if available_collection == ["pseudoValue"] else c
            # species ############
            url = f"{base_url}/{assembly_path}/{r}/fasta/{c}"
            
            with urllib.request.urlopen(url) as response:
                html_content = response.read()

            soup = BeautifulSoup(html_content, "html.parser")
            all_species = [link.text.strip("/") for link in soup.find_all("a") if "/" in link.text and link.text.endswith("/")]

            
            if s not in all_species:
#                if not bac:
                print(colored_failed)
                terminal_width = shutil.get_terminal_size().columns
                species_per_line = terminal_width // max(len(species) for species in all_species)
                print(f"\nAvailable species for assembly '{a}' and release '{r}':")
                for i in range(0, len(all_species), species_per_line):
                    species = "   ".join(all_species[i:i+species_per_line])
                    colored_species = f"{RED}{species}{COLOR_END}"
                    print(colored_species)
                warnings.warn(f"The provided species '{s}' is not available for assembly '{a}' and release '{r}' in Ensembl.", MissingValueCombination)
                continue


            # seq type ###########
            url = f"{base_url}/{assembly_path}/{r}/fasta/{c}/{s}"
            with urllib.request.urlopen(url) as response:
                html_content = response.read()

            soup = BeautifulSoup(html_content, "html.parser")
            all_seqtype = [link.text.strip("/") for link in soup.find_all("a") if "/" in link.text and link.text.endswith("/")]

            if t not in all_seqtype:
                print(colored_failed)
                terminal_width = shutil.get_terminal_size().columns
                seqtype_per_line = terminal_width // max(len(seqtype) for seqtype in all_seqtype)
                print(f"\nAvailable types for assembly '{a}', release '{r}', and species '{s}':")
                for i in range(0, len(all_seqtype), seqtype_per_line):
                    seqtype = "   ".join(all_seqtype[i:i+seqtype_per_line])
                    colored_seqtype = f"{YELLOW}{seqtype}{COLOR_END}"
                    print(colored_seqtype)
                warnings.warn(f"The provided type '{t}' is not available for assembly '{a}', release '{r}', and species '{s}' in Ensembl.", MissingValueCombination)
                continue
            
            url_fasta = f"{base_url}/{assembly_path}/{r}/fasta/{c}/{s}/{t}"
            url_anno = f"{base_url}/{assembly_path}/{r}/gtf/{c}/{s}"

            print(colored_success)
            
            naam = f"{a}_{r}_{s}_{t}"
            results[naam] = {
            'assembly': a,
            'release': r,
            'species': s,
            'seqtype': t,
            'url_fasta': url_fasta,
            'url_anno': url_anno
            }

    return(results)


def combination_checks_refseq(config, base_url):
    """Class to check for the available assemblies/accessions in refseq"""

    accession = config["refseq"]["accession"]
    assembly = config["refseq"]["assembly"]
    species = config["refseq"]["species"]
    seqtype = config["refseq"]["seq_type"]

    results = {}
    
    #for i, x in enumerate(accession):
    for idx, (x, t) in enumerate(product(accession, seqtype)):

        a = assembly[idx].replace(" ", ".")
        s = species[idx].replace(" ", ".")
        t = t.replace(" ", ".")
        
        # Splitting accession
        prefix, numeric_part = x.split('_')
        # get release
        r = numeric_part.split('.')[1].replace(" ", ".")
        # Create the url
        numeric_part = numeric_part.split('.')[0]
        chunks = [numeric_part[:3], numeric_part[3:6], numeric_part[6:]]
        formatted_string = f"{prefix}/{chunks[0]}/{chunks[1]}/{chunks[2]}/"

        url_fasta = base_url + formatted_string + x + '_' + a
        url_anno = base_url + formatted_string + x + '_' + a
        
        print(colored_success)

        naam = f"{a}_{r}_{s}_{t}"
        results[naam] = {
            'assembly': a, 
            'release': r, 
            'species': s, 
            'seqtype': t, 
            'url_fasta': url_fasta, 
            'url_anno': url_anno
            }

    return(results)


def combination_checks_file(config):
    """Class to create a similar dict structure as refseq and ensembl when source == file; no checks"""

    assembly = config["file"]["assembly"]
    release = config["file"]["release"]
    species = config["file"]["species"]
    seqtype = config["file"]["seq_type"]
    
    a = assembly
    r = release
    s = species
    t = seqtype

    naam = f"{a}_{r}_{s}_{t}"

    results = {}
    results[naam] = {
            'assembly': a, 
            'release': r, 
            'species': s, 
            'seqtype': t, 
            'url_fasta': None, 
            'url_anno': None
            }
    
    return(results)