"""Functions for calcualting the memory and runtime after every failed attempt until number of --retries."""

def get_mem_mb(wildcards, attempt, base_mem):
    """Increase memory allocation with each retry attempt."""
    print(attempt)
    print(base_mem)
    return base_mem * attempt

def get_runtime(wildcards, attempt, base_time):
    """Increase runtime allocation with each retry attempt."""
    return f"{base_time * attempt}h"