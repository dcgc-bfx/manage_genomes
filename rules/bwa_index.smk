# bwa ##########
rule bwa_index:
    input:
        fasta_files = os.path.join(fasta_download_path, "{s}", "{a}_{r}_{t}", "genome.fa")
    
    output:
        indices = os.path.join(bwa_indices_path, "{s}", "{a}_{r}_{t}", "{s}.sa")

    wildcard_constraints:
        t = "dna|cdna|cds|ncrna"
    
    params:
        params = config["build_indices"]["bwa"]["tool_params"] if config["build_indices"]["bwa"]["tool_params"] else "",
    
    log:
        os.path.join(bwa_indices_path, "{s}", "{a}_{r}_{t}", "log", "bwa_index.log")
    
    benchmark:
        os.path.join(bwa_indices_path, "{s}", "{a}_{r}_{t}", "benchmark", "bwa_index.txt")
    
    threads: threads
    
    resources:
        cpus = lambda wildcards, threads: threads,
        mem_mb = lambda wildcards, attempt: get_mem_mb(wildcards = wildcards, attempt = attempt, base_mem = resources_mb/8),
        runtime = lambda wildcards, attempt: get_runtime(wildcards = wildcards, attempt = attempt, base_time = 2)

    envmodules:
        format_module_spec('bwa')
    
    shell:
        """
            echo "bwa... {wildcards.s}"
            (
                echo "Making the bwa indices"
                mkdir -p {bwa_indices_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}
                
                bwa index {params.params} -p {bwa_indices_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/{wildcards.s} {input.fasta_files}
            ) &> {log}
        """
