# xengsort ##########
rule xengsort_index:
    input:
        fasta_files_host = os.path.join(fasta_download_path, "{host_s}", "{host_a}_{host_r}_{t}", "genome.fa"),
        fasta_files_graft = os.path.join(fasta_download_path, "{graft_s}", "{graft_a}_{graft_r}_{t}", "genome.fa")

    output:
        indices = os.path.join(xengsort_indices_path, "{host_s}_{host_a}_{host_r}_{t}", "{graft_s}_{graft_a}_{graft_r}_{t}", "index.hash")
    
    wildcard_constraints:
        host_t = "cdna",
        graft_t = "cdna"
    
    params:
        params = config["build_indices"]["xengsort"]["tool_params"] if config["build_indices"]["xengsort"]["tool_params"] else "",
    
    threads: threads
    
    log:
        os.path.join(xengsort_indices_path, "{host_s}_{host_a}_{host_r}_{t}", "{graft_s}_{graft_a}_{graft_r}_{t}", "log", "xengsort_index.log")
    
    benchmark:
        os.path.join(xengsort_indices_path, "{host_s}_{host_a}_{host_r}_{t}", "{graft_s}_{graft_a}_{graft_r}_{t}", "benchmark", "xengsort_index.txt")
    
    threads: threads
    
    resources:
        cpus = lambda wildcards, threads: threads,
        mem_mb = lambda wildcards, attempt: get_mem_mb(wildcards = wildcards, attempt = attempt, base_mem = resources_mb),
        runtime = lambda wildcards, attempt: get_runtime(wildcards = wildcards, attempt = attempt, base_time = 4)
    
    envmodules:
        format_module_spec('xengsort')

    shell:
        """
            echo "Xengsort... {wildcards.host_s} {wildcards.graft_s}"
            (
                pip install jsonargparse
                echo "Making the Xengsort indices"
                current_dir=$(pwd)
                mkdir -p {xengsort_indices_path}/{wildcards.host_s}_{wildcards.host_a}_{wildcards.host_r}_{wildcards.t}/{wildcards.graft_s}_{wildcards.graft_a}_{wildcards.graft_r}_{wildcards.t}
                cd {xengsort_indices_path}/{wildcards.host_s}_{wildcards.host_a}_{wildcards.host_r}_{wildcards.t}/{wildcards.graft_s}_{wildcards.graft_a}_{wildcards.graft_r}_{wildcards.t}
                
                if [ "{params.params}" != "None" ]
                then
                    xengsort index --index index -H input.fasta_files.host -G input.fasta_files.graft {params.params} -W {threads}
                else
                    xengsort index --index index -H input.fasta_files.host -G input.fasta_files.graft -W {threads}
                fi
                
                cd "$current_dir"
            ) &> {log}
        """