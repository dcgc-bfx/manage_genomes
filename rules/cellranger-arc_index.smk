# cellranger-arc ##########
rule cellranger_arc_index:
    input:
        fasta_files = os.path.join(fasta_download_path, "{s}", "{a}_{r}_{t}", "genome.fa"),
        gtf_file = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "annotation_cellranger.gtf")

    output:
        indices = os.path.join(cellranger_indices_path, "{s}", "{a}_{r}_{t}", "reference.json")

    wildcard_constraints:
        t = "dna|cdna|cds|ncrna"
    
    params:
        params = config["build_indices"]["cellranger-arc"]["tool_params"] if config["build_indices"]["cellranger-arc"]["tool_params"] else "",
        tf_motifs_file = config["build_indices"]["cellranger-arc"]["tf_motifs"],
        non_nuclear_contigs = config["build_indices"]["cellranger-arc"]["non_nuclear_contigs"]

    log:
        os.path.join(cellranger_indices_path, "{s}", "{a}_{r}_{t}", "log", "cellranger-arc_index.log")
    
    benchmark:
        os.path.join(cellranger_indices_path, "{s}", "{a}_{r}_{t}", "benchmark", "cellranger-arc_index.txt")

    threads: threads

    resources:
        cpus = lambda wildcards, threads: threads,
        mem_mb = lambda wildcards, attempt: get_mem_mb(wildcards = wildcards, attempt = attempt, base_mem = resources_mb/1.5),
        runtime = lambda wildcards, attempt: get_runtime(wildcards = wildcards, attempt = attempt, base_time = 2)

    envmodules:
        format_module_spec('cellranger')

    singularity: None

    shell:
        """
        set -e
        echo "Cellranger-arc... {wildcards.s}"
        (
            echo "Making the Cellranger-arc indices"

            if [ -z "{params.tf_motifs_file}" ] || [ "{params.tf_motifs_file}" = "None" ] || \
            [ -z "{params.non_nuclear_contigs}" ] || [ "{params.non_nuclear_contigs}" = "None" ]
            then
                cat << EOF > {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/config_tfmotif.json
                {{
                    "organism": "{wildcards.s}",
                    "genome": ["temp"],
                    "input_fasta": ["{input.fasta_files}"],
                    "input_gtf": ["{input.gtf_file}"]
                }}
EOF
            else
                cat << EOF > {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/config_tfmotif.json
                {{
                    "organism": "{wildcards.s}",
                    "genome": ["temp"],
                    "input_fasta": ["{input.fasta_files}"],
                    "input_gtf": ["{input.gtf_file}"],
                    "non_nuclear_contigs": ["{params.non_nuclear_contigs}"],
                    "input_motifs": "{params.tf_motifs_file}"
                }}
EOF
            fi


            tmp_dir=$(mktemp -d -t {wildcards.s}_{wildcards.a}_{wildcards.r}_{wildcards.t}_XXXXXX)
            
            # set so that it will be deleted on exit
            trap 'rm -rf -- "${{tmp_dir}}"' EXIT
            
            cd ${{tmp_dir}}

            /share/apps/cellranger-arc/2.0.2/cellranger-arc mkref --config={annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/config_tfmotif.json --nthreads={threads} {params.params}

            mkdir -p {cellranger_indices_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}
            mv ${{tmp_dir}}/temp/* {cellranger_indices_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/
        
        ) &> {log}
        """
