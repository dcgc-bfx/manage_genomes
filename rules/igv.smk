# igv file
rule create_igv_file:
    input:
        gtf_file = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "annotation.gtf")
    
    output:
        temp_file = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "annotation.igv.gtf"),
        igv_file = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "annotation.igv.gtf.idx")

    log:
        os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "log", "igv.log")

    benchmark:
        os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "benchmark", "igv.txt")
    
    threads: 1
    
    resources:
        cpus = lambda wildcards, threads: threads,
        mem_mb = lambda wildcards, attempt: get_mem_mb(wildcards = wildcards, attempt = attempt, base_mem = resources_mb/9),
        runtime = lambda wildcards, attempt: get_runtime(wildcards = wildcards, attempt = attempt, base_time = 0.5)
    
    envmodules:
        format_module_spec('java')
    
    shell:
        """
            echo "IGV... {wildcards.s}"
            (
                igvtools sort {input.gtf_file} {output.temp_file}
		igvtools index {output.temp_file}
            ) &> {log}
        """
