# kallisto ##########
rule kallisto_index:
    input:
        fasta_file = os.path.join(fasta_download_path, "{s}", "{a}_{r}_{t}", "genome.fa"),
        fasta_index_file = os.path.join(fasta_download_path, "{s}", "{a}_{r}_{t}", "genome.fa.fai"),
        bed_file = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "annotation.bed")
            
    output:
        indices = os.path.join(kallisto_indices_path, "{s}", "{a}_{r}_{t}", "{s}.idx")

    wildcard_constraints:
        t = "dna|cdna|cds|ncrna"
    
    params:
        params = config["build_indices"]["kallisto"]["tool_params"] if config["build_indices"]["kallisto"]["tool_params"] else "",

    log:
        os.path.join(kallisto_indices_path, "{s}", "{a}_{r}_{t}", "log", "kallisto_index.log")
    
    benchmark:
        os.path.join(kallisto_indices_path, "{s}", "{a}_{r}_{t}", "benchmark", "kallisto_index.txt")
    
    threads: threads
    
    resources:
        cpus = lambda wildcards, threads: threads,
        mem_mb = lambda wildcards, attempt: get_mem_mb(wildcards = wildcards, attempt = attempt, base_mem = resources_mb/5),
        runtime = lambda wildcards, attempt: get_runtime(wildcards = wildcards, attempt = attempt, base_time = 1)
    
    envmodules:
        format_module_spec('kallisto', 'bedtools')
    
    shell:
        """
            echo "Kallisto... {wildcards.s}"
            (
                echo "Making the kallisto indices"
                
                mkdir -p {kallisto_indices_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}
                
                tmp_dir=$(mktemp -d -t {wildcards.s}_{wildcards.a}_{wildcards.r}_{wildcards.t}_XXXXXX)
                
                # set so that it will be deleted on exit
                trap 'rm -rf -- "${{tmp_dir}}"' EXIT

                bedtools getfasta -s -split -nameOnly -fi {input.fasta_file} -fo "$tmp_dir/transcripts.fa" -bed {input.bed_file}
                perl -pi -e 'if(m/>/){{ s/\([+-]\)$// }}' "$tmp_dir/transcripts.fa"

                kallisto index -i {output.indices} {params.params} -t {threads} $tmp_dir/transcripts.fa
                
#                rm -rf $tmp_dir
            ) &> {log}
        """
