# refflat file
rule create_refFlat_file:
    input:
        gtf_file = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "annotation.gtf")
    
    output:
        rf_file = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "annotation.refFlat")

    log:
        os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "log", "refFlat.log")

    benchmark:
        os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "benchmark", "refFlat.txt")
    
    threads: 1
    
    resources:
        cpus = lambda wildcards, threads: threads,
        mem_mb = lambda wildcards, attempt: get_mem_mb(wildcards = wildcards, attempt = attempt, base_mem = resources_mb/50),
        runtime = lambda wildcards, attempt: get_runtime(wildcards = wildcards, attempt = attempt, base_time = 1)
    
    envmodules:
        format_module_spec('ucsctools')
    
    shell:
        """
            echo "refFlat... {wildcards.s}"
            (
                set -x
                
                gtfToGenePred -genePredExt -ignoreGroupsWithoutExons {input.gtf_file} stdout | awk 'BEGIN{{FS=OFS="\\t"}}{{print $12,$1,$2,$3,$4,$5,$6,$7,$8,$9,$10}}' > {output.rf_file}
                
                set +x
            ) &> {log}
        """
