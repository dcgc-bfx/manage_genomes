# cellranger gtf ######
rule create_cellranger_gtf_file:
    input:
        gtf_file = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "annotation.gtf"),
        gtf_status = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "gtf_validator.txt")

    output:
        cellranger_gtf_file = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "annotation_cellranger.gtf")
    
    log:
        os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "log", "cellranger_gtf.log")
    
    benchmark:
        os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "benchmark", "cellranger_gtf.txt")

    threads: 1

    resources:
        cpus = lambda wildcards, threads: threads,
        mem_mb = lambda wildcards, attempt: get_mem_mb(wildcards = wildcards, attempt = attempt, base_mem = resources_mb/70),
        runtime = lambda wildcards, attempt: get_runtime(wildcards = wildcards, attempt = attempt, base_time = 0.5)
    
    envmodules:
        format_module_spec('cellranger')

    singularity: None

    shell:
        """
            echo "Cellranger gtf... {wildcards.s}"
            (
                echo "Making the Cellranger gtf"

                /share/apps/cellranger-arc/2.0.2/cellranger-arc mkgtf {input.gtf_file} {output.cellranger_gtf_file} \
                --attribute=gene_biotype:protein_coding --attribute=gene_biotype:lincRNA --attribute=gene_biotype:antisense \
                --attribute=gene_biotype:IG_LV_gene --attribute=gene_biotype:IG_V_gene --attribute=gene_biotype:IG_V_pseudogene \
                --attribute=gene_biotype:IG_D_gene --attribute=gene_biotype:IG_J_gene --attribute=gene_biotype:IG_J_pseudogene \
                --attribute=gene_biotype:IG_C_gene --attribute=gene_biotype:IG_C_pseudogene --attribute=gene_biotype:TR_V_gene \
                --attribute=gene_biotype:TR_V_pseudogene --attribute=gene_biotype:TR_D_gene --attribute=gene_biotype:TR_J_gene \
                --attribute=gene_biotype:TR_J_pseudogene --attribute=gene_biotype:TR_C_gene
            ) &> {log}
        """