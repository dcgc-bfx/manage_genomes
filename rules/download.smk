import urllib.request
import re

rule download_annotation:
    output:
        os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "annotation.gtf"),
    
    log:
        os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "log", "download-annotation.log")

    benchmark:
        os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "benchmark", "download-annotation.txt")

    threads: 1
    
    resources:
        cpus = lambda wildcards, threads: threads,
        mem_mb = lambda wildcards, attempt: get_mem_mb(wildcards = wildcards, attempt = attempt, base_mem = resources_mb/45),
        runtime = lambda wildcards, attempt: get_runtime(wildcards = wildcards, attempt = attempt, base_time = 1)

    run:
        if (config["source"] == "ensembl" or config["source"] == "refseq"):

            all_keys = list(downloadable_files.keys())

            naam = f"{wildcards.r}_{wildcards.s}"

            regex = re.compile(naam)
            matched_keys = [key for key in all_keys if regex.search(key)]

            url_anno = downloadable_files[matched_keys[0]]["url_anno"]
            anno_files = downloadable_files[matched_keys[0]]["annotation"]
            
            readme_file_path = f"{annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/readme.txt"

            with open(readme_file_path, 'w') as readme_file:
                for a_file in anno_files:
                    url = f"{url_anno}/{a_file}"
                    filename = f"{annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/{a_file}"
                    urllib.request.urlretrieve(url, filename)
                    readme_file.write(url + '\n')


        if (config["source"] == "ensembl"):
            if config["ensembl"]["annotation"]["download"]:
                shell(
                    """
                        echo "Download ensembl annotation... {wildcards.s}"
                        (
                            ls -lha {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/*.gtf.gz
                            ls -lha {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/*.gtf.gz >> {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/readme.txt
                            zcat {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/*.gtf.gz > {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/annotation.gtf
                            # Remove trailing white spaces
                            sed -i 's/[[:space:]]\+$//' {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/annotation.gtf
                            rm -rf {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/*.gtf.gz
                        ) &> {log}
                        echo "Downloaded ensembl annotation... {wildcards.s}"
                    """
                    )
        elif (config["source"] == "refseq"):
            if config["refseq"]["annotation"]["download"]:
                shell(
                    """
                        echo "Download refseq annotation... {wildcards.s}"
                        (
                            ls -lha {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/*.gtf.gz
                            ls -lha {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/*.gtf.gz >> {annotation_download_path}/{species}/{assembly}_{release}_annotation/readme.txt
                            zcat {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/*.gtf.gz > {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/annotation.gtf
                            rm -rf {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/*.gtf.gz

                            # Remove trailing white spaces
                            sed -i 's/[[:space:]]\+$//' {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/annotation.gtf
                            # Remove the pattern `transcript_id ""; ` for the gene entries
                            sed -i '/^[^\t]*\t[^\t]*\tgene\t/ s/transcript_id ""; //g' {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/annotation.gtf
                            
                            
                            # Remove the pattern `incomplete; ` from all lines. The entries that do not have 2 values fail for collapse_annotation.py for rnaseqc
                            # sed -i 's/incomplete; //g' {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/annotation.gtf
                            # Remove the pattern `.; ` from all lines. The entries that do not have 2 values fail for collapse_annotation.py for rnaseqc
                            # sed -i 's/\.; //g' {annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/annotation.gtf
                        ) &> {log}
                        echo "Downloaded refseq annotation... {wildcards.s}"
                    """
                    )
                
                result = clean_refseq_gtf(f"{annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/annotation.gtf")

                with open(f"{annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation/annotation.gtf", "w") as f:
                    for line in result:
                        formatted_line = '\t'.join(map(str, line)).replace('  ', ' ')
                        formatted_line = re.sub(r'extra \$$', '', formatted_line)
                        f.write(f"{formatted_line}\n")
                f.close()
        elif config["source"] == "file":
            shell(
                """
                    echo "Copying annotation... {wildcards.s}"
                    (
                        # Check if the gtf_path is defined
                        if [ -n "{config[file][gtf_path]}" ]; then

                            gtf_path="{config[file][gtf_path]}"
                            output_dir="{annotation_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_annotation"
                            mkdir -p "$output_dir"


                            # List the fasta file details into the readme.txt
                            ls -lha "$gtf_path"
                            ls -lha "$gtf_path" >> "$output_dir/readme.txt"

                            # Check if the gtf file ends with .gz
                            if [[ "$gtf_path" == *.gz ]]; then
                                # Remove .gz extension and decompress the file
                                decompressed_gtf="${{gtf_path%.gz}}"
                                zcat "$gtf_path" > "$decompressed_gtf"
                                rm -rf "$output_dir/annotation.gtf"
                                cp "$decompressed_gtf" "$output_dir/annotation.gtf"
                            else
                                # Directly copy the gtf file if it's not gzipped
                                rm -rf "$output_dir/annotation.gtf"
                                cp "$gtf_path" "$output_dir/annotation.gtf"
                            fi
                        fi

                    ) &> {log}
                    echo "Copied annotation... {wildcards.s}"
                """
            )



rule download_genome:
    output:
        os.path.join(fasta_download_path, "{s}", "{a}_{r}_{t}", "genome.fa")
        
    wildcard_constraints:
        t = "dna|cdna|cds|ncrna|pep|dna_index"
    
    log:
        os.path.join(fasta_download_path, "{s}", "{a}_{r}_{t}", "log", "download-genome.log")

    benchmark:
        os.path.join(fasta_download_path, "{s}", "{a}_{r}_{t}", "benchmark", "download-genome.txt")

    threads: 1
    
    resources:
        cpus = lambda wildcards, threads: threads,
        mem_mb = lambda wildcards, attempt: get_mem_mb(wildcards = wildcards, attempt = attempt, base_mem = resources_mb/28),
        runtime = lambda wildcards, attempt: get_runtime(wildcards = wildcards, attempt = attempt, base_time = 0.5)

    run:
        if (config["source"] == "ensembl" or config["source"] == "refseq"):
            all_keys = list(downloadable_files.keys())

            naam = f"{wildcards.r}_{wildcards.s}_{wildcards.t}"

            regex = re.compile(naam)
            matched_keys = [key for key in all_keys if regex.search(key)]

            url_fasta = downloadable_files[matched_keys[0]]["url_fasta"]
            fasta_files = downloadable_files[matched_keys[0]]["fasta"]
            
            readme_file_path = f"{fasta_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/readme.txt"

            with open(readme_file_path, 'w') as readme_file:
                for f_file in fasta_files:
                    url = f"{url_fasta}/{f_file}"
                    filename = f"{fasta_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/{f_file}"
                    urllib.request.urlretrieve(url, filename)
                    readme_file.write(url + '\n')


        if (config["source"] == "ensembl"):
            if (config["ensembl"]["fasta"]["download"]):
                shell(
                    """
                        echo "Download ensembl fasta... {wildcards.s}"
                        (
                            ls -lha {fasta_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/*.fa.gz
                            ls -lha {fasta_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/*.fa.gz >> {fasta_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/readme.txt
                            zcat {fasta_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/*.fa.gz > {fasta_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/genome.fa
                            rm -rf {fasta_download_path}/{species}/{assembly}_{release}_{seqtype}/*.fa.gz
                        ) &> {log}
                        echo "Downloaded ensembl fasta... {wildcards.s}"
                    """
                    )
        elif (config["source"] == "refseq"):
            if (config["refseq"]["fasta"]["download"]):
                shell(
                    """
                        echo "Download refseq fasta... {wildcards.s}"
                        (
                            ls -lha {fasta_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/*.fna.gz
                            ls -lha {fasta_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/*.fna.gz >> {fasta_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/readme.txt
                            zcat {fasta_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/*.fna.gz > {fasta_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/genome.fa
                            rm -rf {fasta_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/*.fna.gz
                        ) &> {log}
                        echo "Downloaded refseq fasta... {wildcards.s}"
                    """
                    )
        elif (config["source"] == "file"):
            shell(
                """
                    set -x
                    echo "Copying fasta... {wildcards.s}"
                    (
                        # Check if the fasta_path is defined
                        if [ -n "{config[file][fasta_path]}" ]; then

                            fasta_path="{config[file][fasta_path]}"
                            output_dir="{fasta_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}"
                            mkdir -p "$output_dir"


                            # Log the path
                            echo {fasta_download_path}

                            # List the fasta file details into the readme.txt
                            ls -lha "$fasta_path" >> "$output_dir/readme.txt"

                            # Check if the fasta file ends with .gz
                            if [[ "$fasta_path" == *.gz ]]; then
                                # Remove .gz extension and decompress the file
                                decompressed_fasta="${{fasta_path%.gz}}"
                                zcat "$fasta_path" > "$decompressed_fasta"
                                rm -rf "$output_dir/genome.fa"
                                cp "$decompressed_fasta" "$output_dir/genome.fa"
                            else
                                # Directly copy the fasta file if it's not gzipped
                                rm -rf "$output_dir/genome.fa"
                                cp "$fasta_path" "$output_dir/genome.fa"
                            fi
                        fi

                    ) &> {log}
                    echo "Copied fasta... {wildcards.s}"
                    set +x
                """
                )



rule download_other_ensembl:
    output:
        os.path.join(other_download_path, "{s}", "{a}_{r}_other", "{r_file}")

    wildcard_constraints:
        r_file = ".*.txt.gz",

    log:
        os.path.join(other_download_path, "{s}", "{a}_{r}_other", "log", "{r_file}.log")
   
    benchmark:
        os.path.join(other_download_path, "{s}", "{a}_{r}_other", "benchmark", "{r_file}.txt")

    threads: 1

    resources:
        cpus = lambda wildcards, threads: threads,
        mem_mb = 8000,
        runtime = '8h'
    
    run:
        if config["ensembl"]["other"]["download"]:
            for key, values in downloadable_files.items():
                assembly = values["assembly"]
                release = values["release"]
                species = values["species"]
                other_files = values["other"]

                for o_file in other_files:
                    url = f"{base_url}/{assembly_path}/{release}/mysql/{species}_core_{release}_*/{seqtype}/{g_file}"
                    filename = os.path.join(other_download_path, species, assembly + "_" + release + "_" + other, o_file)
                    urllib.request.urlretrieve(url, filename)
                    shell(
                    """
                        path=$(dirname {filename})
                        unzip {filename} -d "$path"
                    """
                    )



rule gtf_validator:
    input:
        gtf = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "annotation.gtf")

    output:
        status = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "gtf_validator.txt")
    
    log:
        os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "log", "gtf_validator.log")

    benchmark:
        os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "benchmark", "gtf_validator.txt")

    threads: 1
    
    resources:
        cpus = lambda wildcards, threads: threads,
        mem_mb = lambda wildcards, attempt: get_mem_mb(wildcards = wildcards, attempt = attempt, base_mem = resources_mb/15),
        runtime = lambda wildcards, attempt: get_runtime(wildcards = wildcards, attempt = attempt, base_time = 0.5)

    shell:
        """
            (
                rm -rf {output.status}
                Rscript "{snakefile_dir}"/scripts/gtf_validator.R --input {input.gtf} --output {output.status}
            ) &> {log}
        """