# bed file ###
rule create_bed_file:
    input:
        gtf_file = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "annotation.gtf")
    
    output:
        bed_file = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "annotation.bed")

    log:
        os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "log", "bed.log")

    benchmark:
        os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "benchmark", "bed.txt")

    threads: threads

    resources:
        cpus = lambda wildcards, threads: threads,
        mem_mb = lambda wildcards, attempt: get_mem_mb(wildcards = wildcards, attempt = attempt, base_mem = resources_mb/40),
        runtime = lambda wildcards, attempt: get_runtime(wildcards = wildcards, attempt = attempt, base_time = 0.5)
    
    envmodules:
        format_module_spec('ucsctools')

    shell:
        """
            (
                set -x
                
                gtfToGenePred -ignoreGroupsWithoutExons {input.gtf_file} stdout | genePredToBed stdin stdout > {output.bed_file}
                
                set +x
            ) &> {log}
        """
