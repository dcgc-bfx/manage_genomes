rule create_ancillary_files:
    input:
        os.path.join(fasta_download_path, "{s}", "{a}_{r}_{t}", "genome.fa")
    
    output:
        os.path.join(fasta_download_path, "{s}", "{a}_{r}_{t}", "genome.fa.fai"),
        os.path.join(fasta_download_path, "{s}", "{a}_{r}_{t}", "genome.dict"),
        os.path.join(fasta_download_path, "{s}", "{a}_{r}_{t}", "genome.genome")
        
    wildcard_constraints:
        t = "dna|cdna|cds|ncrna|pep|dna_index"
    
    log:
        os.path.join(fasta_download_path, "{s}", "{a}_{r}_{t}", "log", "ancillary-files.log")
    
    benchmark:
        os.path.join(fasta_download_path, "{s}", "{a}_{r}_{t}", "benchmark", "ancillary-files.txt")

    threads: 1
    
    resources:
        cpus = lambda wildcards, threads: threads,
        mem_mb = lambda wildcards, attempt: get_mem_mb(wildcards = wildcards, attempt = attempt, base_mem = resources_mb/3),
        runtime = lambda wildcards, attempt: get_runtime(wildcards = wildcards, attempt = attempt, base_time = 2)

    envmodules:
        format_module_spec('samtools', 'picard')

    shell:
        '''
            echo "Ancillary files... {wildcards.s}"
            (
                cd {fasta_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/
                echo {fasta_download_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/

                samtools faidx genome.fa

                cut -f 1,2 genome.fa.fai > genome.genome

                rm -rf genome.dict

                picard CreateSequenceDictionary -R genome.fa -O genome.dict -SPECIES {wildcards.s} -GENOME_ASSEMBLY {wildcards.a}
            ) &> {log}
        '''
