# gene-transcript-relation file ####
rule create_gene_transcript_file:
    input:
        gtf_file = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "annotation.gtf")

    output:
        gt_file = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "gene-transcript.txt")

    log:
        os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "log", "gene-transcript.log")

    benchmark:
        os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "benchmark", "gene-transcript.txt")
    
    threads: 1
    
    singularity: "/dcgc/support/pipeline/singularity/singularity-base-r_v1.1.0.sif"

    resources:
        cpus = lambda wildcards, threads: threads,
        mem_mb = lambda wildcards, attempt: get_mem_mb(wildcards = wildcards, attempt = attempt, base_mem = resources_mb/35),
        runtime = lambda wildcards, attempt: get_runtime(wildcards = wildcards, attempt = attempt, base_time = 0.5)

    envmodules:
        format_module_spec('R')
    
    shell:
        """
            echo "Gene transcript... {wildcards.s}"
            (
                # Check conditions for running biomaRt
                if [[ "{source}" == "ensembl" && "{database}" != "bacteria" ]]; then
                    echo "Running biomaRt"
                    Rscript {snakefile_dir}/scripts/biomaRt.R --db {database} --species {wildcards.s} --release {wildcards.r} --out {output.gt_file}
                    ls -lh {output.gt_file}
                    wc -l {output.gt_file}
                else
                    touch {output.gt_file}
                fi

                # Count the number of lines in the output file
                num_lines=$(wc -l < {output.gt_file})

                # Determine if gtf processing is needed
                if [[ "$num_lines" -lt 2 ]]; then
                    gtf=true
                else
                    gtf=false
                fi

                if [[ ( "{source}" == "refseq" || "{source}" == "file" || "$gtf" == true ) && "{database}" != "bacteria" ]]; then
                    echo "Trying from gtf"
                    Rscript "{snakefile_dir}"/scripts/gene_transcript.R --input {input.gtf_file} --output {output.gt_file}
                elif [[ "{database}" == "bacteria" ]]; then
                    echo "Bacteria"
                    Rscript "{snakefile_dir}"/scripts/gene_transcript.R --input {input.gtf_file} --type {database} --output {output.gt_file}
                fi

            ) &> {log}
        """
