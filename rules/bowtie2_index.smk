import os

# bowtie2 ##########
rule bowtie2_index:
    input:
        fasta_files = os.path.join(fasta_download_path, "{s}", "{a}_{r}_{t}", "genome.fa")

    output:
        indices = [os.path.join(bowtie2_indices_path, "{s}", "{a}_{r}_{t}", "{s}.4.{ext}")] + 
                    [os.path.join(bowtie2_indices_path, "{s}", "{a}_{r}_{t}", "{s}.rev.2.{ext}")]

    wildcard_constraints:
        t = "dna|cdna|cds|ncrna",
        ext = "bt2|bt2l"
    
    params:
        params = config["build_indices"]["bowtie2"]["tool_params"] if config["build_indices"]["bowtie2"]["tool_params"] else "",

    log:
        os.path.join(bowtie2_indices_path, "{s}", "{a}_{r}_{t}", "log", "bowtie2_index_{ext}.log")
    
    benchmark:
        os.path.join(bowtie2_indices_path, "{s}", "{a}_{r}_{t}", "benchmark", "bowtie2_index_{ext}.txt")
    
    threads: threads

    resources:
        cpus = lambda wildcards, threads: threads,
        mem_mb = lambda wildcards, attempt: get_mem_mb(wildcards = wildcards, attempt = attempt, base_mem = resources_mb/3),
        runtime = lambda wildcards, attempt: get_runtime(wildcards = wildcards, attempt = attempt, base_time = 1)
    
    envmodules:
        format_module_spec('bowtie2')
    
    shell:
        """
            echo "bowtie2... {wildcards.s}"
            (
                echo "Making the bowtie2 indices"
                mkdir -p {bowtie2_indices_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}
                
                bowtie2-build {params.params} --large-index --threads {threads} {input.fasta_files} {bowtie2_indices_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}/{wildcards.s}
            ) &> {log}
        """
