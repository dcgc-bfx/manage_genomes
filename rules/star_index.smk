# STAR ##########
rule star_index:
    input:
        fasta_files = os.path.join(fasta_download_path, "{s}", "{a}_{r}_{t}", "genome.fa"),
        gtf_file = os.path.join(annotation_download_path, "{s}", "{a}_{r}_annotation", "annotation.gtf")
        
    output:
        indices = (os.path.join(star_indices_path, "{s}", "{a}_{r}_{t}", "SA"))

    wildcard_constraints:
        t = "dna|cdna|cds|ncrna"
    
    params:
        params = config["build_indices"]["star"]["tool_params"] if config["build_indices"]["star"]["tool_params"] else "",
    
    log:
        os.path.join(star_indices_path, "{s}", "{a}_{r}_{t}", "log", "star_index.log")
    
    benchmark:
        os.path.join(star_indices_path, "{s}", "{a}_{r}_{t}", "benchmark", "star_index.txt")
    
    threads: threads
    
    resources:
        cpus = lambda wildcards, threads: threads,
        mem_mb = lambda wildcards, attempt: get_mem_mb(wildcards = wildcards, attempt = attempt, base_mem = resources_mb),
        runtime = lambda wildcards, attempt: get_runtime(wildcards = wildcards, attempt = attempt, base_time = 4)

    
    envmodules:
        format_module_spec('star')

    shell:
        """
            echo "STAR... {wildcards.s}"
            (
                echo "Making the STAR indices"

                # Check if genomeSAindexNbases is not already defined in params
                if [[ ! "{params.params}" == *"--genomeSAindexNbases"* ]]
                then
                    # Calculate the genome length
                    genome_length=$(grep -v "^>" {input.fasta_files} | sed 's/\\s//g' | awk '{{total += length}} END {{print total}}')
                
                    if [ "$genome_length" -gt 0 ]
                    then
                        # Calculate the log2 of the genome length and determine the index base value
                        log2_genome_length=$(awk -v len="$genome_length" 'BEGIN {{ print log(len)/log(2) }}')
                        genomeSAindexNbases=$(awk -v log2_len="$log2_genome_length" 'BEGIN {{ print int(log2_len/2 - 1) }}')

                        # Ensure the result is not greater than 14
                        if [ "$genomeSAindexNbases" -gt 14 ]
                        then
                            genomeSAindexNbases=14
                        fi

                        # Append the calculated genomeSAindexNbases to params
                        params_with_index="{params.params} --genomeSAindexNbases $genomeSAindexNbases"

                        echo "Total genome length: $genome_length"
                        echo "Using genomeSAindexNbases: $genomeSAindexNbases"
                    else
                        echo "Error calculating genome length; alternatively define --genomeSAindexNbases in the config file"
                        exit 1
                    fi
                else
                    params_with_index="{params.params}"
                    echo "Using the 'genomeSAindexNbases' as defined in the config."
                fi

                # Create output directory
                output_dir={star_indices_path}/{wildcards.s}/{wildcards.a}_{wildcards.r}_{wildcards.t}
                mkdir -p $output_dir

                STAR --runMode genomeGenerate --runThreadN {threads} --genomeDir $output_dir --genomeFastaFiles {input.fasta_files} --sjdbGTFfile {input.gtf_file} $params_with_index
            ) &> {log}
        """
